var express = require('express');
var config = require('config');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');
var mongoose = require('mongoose');
var sesion = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var validation = require('express-validator');
var MongoStore = require('connect-mongo')(sesion);


var routes = require('./routes/index');
var routesUser = require('./routes/user');
var Category = require('./models/category');
var routesAdmin = require('./routes/admin');


var app = express();

mongoose.connect('mongodb://slavik:Slavik2000@ds235833.mlab.com:35833/shops', {useNewUrlParser: true});
require('./config/passport');

// view engine setup
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', '.hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(validation());
app.use(cookieParser());
app.use(sesion({
    secret: 'mysecret',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    res.locals.login = req.isAuthenticated();

    res.locals.session = req.session;
    if (req.isAuthenticated()) {
        res.locals.name = req.user.name;
        res.locals.isAdmin = req.user.admin;
    }
    if (req.session.currency === undefined) {
        req.session.currency = 'USD';

    }
    if (req.session.currency === 'USD') {
        res.locals.usd = true;
        res.locals.eur = false;
    } else {
        res.locals.usd = false;
        res.locals.eur = true;
    }

    Category.find(function (err, doc) {
        res.locals.category = doc;
        next();
    });
});

app.use('/user', routesUser);
app.use('/admin', routesAdmin);
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
