var Product = require('../models/product');
var Category = require('../models/category');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/shops', {useNewUrlParser: true});
var category = [
    new Category({
        name: 'Nature'
    }),
    new Category({
        name: 'Car'
    }),
    new Category({
        name: 'other category'
    }),

];
var done = 0;
for (var i = 0; i < category.length; i++) {
    category[i].save(function (err, result) {
        done++;
        if (done == category.length) {
            exit();
        }
    });
}
var product = [
    new Product({
        imagePath: "https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/img.jpg",
        title: "some title3",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et " +
            "dolore magna aliqua.dfg Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ",
        price: 60,
        category: category[0]
    }),
    new Product({
        imagePath: "http://www.nustudent.com/images/pictures/5.jpg",
        title: "Nature",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
        price: 150,
        category: category[0]
    }),
    new Product({
        imagePath: "https://upload.wikimedia.org/wikipedia/commons/f/f9/Phoenicopterus_ruber_in_S%C3%A3o_Paulo_Zoo.jpg",
        title: "Paulo Zoo",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
        price: 50,
        category: category[0]
    }),
    new Product({
        imagePath: "https://orig00.deviantart.net/b712/f/2014/161/d/1/kion_the_lion_guard_by_juffs-d7lshfi.jpg",
        title: "Siomba",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
        price: 550,
        category: category[2]
    }),
    new Product({
        imagePath: "http://i.infocar.ua/i/2/5426/82791/1920x.jpg",
        title: "Camry",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
        price: 11500,
        category: category[1]
    }),
    new Product({
        imagePath: "https://lh4.googleusercontent.com/-RWyKwYJJ3Ak/TXjYm5uMF_I/AAAAAAAAAGc/oV9QIFkjwLU/s1600/dice+with+question+mark.jpg",
        title: "Other",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et ",
        price: 1500,
        category: category[2]
    }),
];
done = 0;
for (var i = 0; i < product.length; i++) {
    product[i].save(function (err, result) {
        done++;
        if (done == product.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();

}
