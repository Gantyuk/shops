var mongoose = require('mongoose');

var User = require('../models/user');

mongoose.connect('mongodb://localhost/shop', {useNewUrlParser: true});

var newUser = new User();
newUser.email = 'admin@admin.com';
newUser.name = 'admin';
newUser.password = newUser.encryptPassword('password');
newUser.admin = true;
newUser.save(function (err, result) {
    mongoose.disconnect();
});