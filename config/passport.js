var passport = require('passport');
var User = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local.signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('name', 'Feld name is empty').notEmpty();
    req.checkBody('password', 'Invalid pasword').notEmpty().isLength({min: 6});
    var errpors = req.validationErrors();
    if (errpors) {
        var messages = [];
        errpors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'email': email}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (user) {
            return done(null, false, {message: "email is already in user."});
        }
        var newUser = new User();
        newUser.email = email;
        newUser.name = req.body.name;
        newUser.password = newUser.encryptPassword(password);
        newUser.save(function (err, result) {
            if (err) {
                return done(err);
            }
            return done(null, newUser);
        });
    })
}));

passport.use('local.signin', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid pasword').notEmpty();
    var errpors = req.validationErrors();
    if (errpors) {
        var messages = [];
        errpors.forEach(function (error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'email': email}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, {message: "no user found"});
        }
        if (!user.validPassword(password)) {
            return done(null, false, {messages: 'Wrong password'});
        }
        return done(null, user);
    })

}));

passport.use(new FacebookStrategy({
    clientID: 791020357734995,
    clientSecret: '10bbe567b637d1c128a70188dc87efec',
    callbackURL: "https://shops-slavik.herokuapp.com/user/facebook/callback",
    profileFields: ['id', 'displayName', 'email']
}, function (accessToken, refreshToken, profile, done) {
    User.findOne({'email': profile.emails[0].value}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (user) {
            return done(null, user);
        }
        else {
            var newUser = new User();
            newUser.email = profile.emails[0].value;
            newUser.name = profile.displayName;
            newUser.password = 'from facebook';
            newUser.save(function (err,) {
                if (err) throw err;
                return done(null, newUser);

            });
        }
    });

}));

passport.use(new GoogleStrategy({
    clientID: '445710308837-1n949di016pcfbb0i64dc61oor731i0c.apps.googleusercontent.com',
    clientSecret: 'r_sTRQvq-Zd5AxNPONiMMxom',
    callbackURL: "https://shops-slavik.herokuapp.com/user/google/callback",
    profileFields: ['id', 'displayName', 'email']
}, function (accessToken, refreshToken, profile, done) {
    User.findOne({'email': profile.emails[0].value}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (user) {
            return done(null, user);
        }
        else {
            var newUser = new User();
            newUser.email = profile.emails[0].value;
            newUser.name = profile.displayName;
            newUser.password = 'from google';
            newUser.save(function (err,) {
                if (err) throw err;
                return done(null, newUser);

            });
        }
    });

}));
