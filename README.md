# NODE.JS SHOP

## Download and Installation

To begin using this project, choose one of the following options to get started:

* Clone the repo: `git clone https://Gantyuk@bitbucket.org/Gantyuk/shops.git`
* [Fork, Clone, or Download on Bitbucket](https://bitbucket.org/Gantyuk/shops/src/master/)

After Download, Clone ... you need to do this command ` npm install`

In order to start seed for db do it  :
* `cd seed`;
* `node ProductSeeder.js`;

That's all, run the node project `npm start` in root folder,enjoy it.