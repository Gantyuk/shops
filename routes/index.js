var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var Cart = require('../models/cart');

var Product = require('../models/product');
var Category = require('../models/category');
var Order = require('../models/order');
var fixer = require('fixer-api');
var csrfProtection = csrf();
router.use(csrfProtection);

var cur;
var sign;

/* GET home page. */
router.get('/', function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, doc) {
        req.session.oldUrlCart = req.url;
        res.render('shop/index', {
            title: 'Shopping Cart',
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
            csrfToken: req.csrfToken(),
            categories: doc,
            active: '#mens'
        });
    });
});
router.get('/category/:active', function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, doc) {
        req.session.oldUrlCart = req.url;
        var active_second_cat, active_3_cat;
        if (req.params.active.split('-').length > 1) {
            active_second_cat = "#" + req.params.active;

        } else {
            active_second_cat = false;
        }
        if (req.params.active.split('-').length > 2) {
            active_3_cat = "#" + req.params.active.split('-')[0] + '-' + req.params.active.split('-')[1];
        } else {
            active_3_cat = false;
        }
        res.render('shop/index', {
            title: 'Shopping Cart',
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
            csrfToken: req.csrfToken(),
            categories: doc,
            active: "#" + req.params.active.split('-')[0],
            active_second_cat: active_second_cat,
            active_3_cat: active_3_cat
        });
    });
});
router.get('/products/:id', function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Product.find({primary_category_id: req.params.id}, function (err, docs) {
        var productChumks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChumks.push(docs.slice(i, i + chunkSize));
        }
        fixer.latest({access_key: '3a90f49f66c40c0abe459df4dbac3bfe'})
            .then(function (data) {
                if (req.session.currency === "USD") {
                    cur = 1;
                    sign = '$';
                } else if (req.session.currency === "EUR") {
                    cur = 1 / data.rates['USD'];
                    sign = '€';
                }
                productChumks.forEach(function (products) {
                    products.forEach(function (product) {
                        product.price = product.price * cur;
                        product.price = product.price.toFixed(2);
                        product.cur = sign;
                        product.imagePath = product.image_groups[0].images[0].link;
                    });
                });
            }).then(function () {
            var breadcrumbs = req.params.id.split('-');
            req.session.oldUrlCart = req.url;
            res.render('shop/products', {
                title: 'Shopping Cart',
                products: productChumks,
                succesMsg: succesMsg,
                noSuccesMsg: !succesMsg,
                csrfToken: req.csrfToken(),
                breadcrumbs: breadcrumbs[0] + "-" + breadcrumbs[1],
                breadcrumbs_name: breadcrumbs[0] + " " + breadcrumbs[1],
                second_breadcrumbs_name: breadcrumbs[2],

            });
        });
    });
});
router.get('/product/:id', function (req, res, next) {

    Product.findById(req.params.id, function (err, docs) {
        var larg = [], smal = [];
        docs.image_groups.forEach(function (value) {
            if (value.view_type === "large") {
                larg = value.images;
            }
            if (value.view_type === "small") {
                smal = value.images;
            }
        });
        fixer.latest({access_key: '3a90f49f66c40c0abe459df4dbac3bfe'})
            .then(function (data) {
                if (req.session.currency === "USD") {
                    cur = 1;
                    sign = '$';
                } else if (req.session.currency === "EUR") {
                    cur = 1 / data.rates['USD'];
                    sign = '€';
                }
                docs.price = (docs.price * cur).toFixed(2);
                docs.cur = sign;
            }).then(function () {
            console.log(docs);
            var breadcrumbs = docs.primary_category_id.split('-');
            req.session.oldUrlCart = req.url;
            res.render('shop/product', {
                title: 'Shopping Cart',
                product: docs,
                smal: smal,
                large: larg[0],
                csrfToken: req.csrfToken(),
                breadcrumbs: breadcrumbs[0] + "-" + breadcrumbs[1],
                breadcrumbs_name: breadcrumbs[0] + " " + breadcrumbs[1],
                second_breadcrumbs_name: breadcrumbs[2],
            });
        });
    });
});

router.get("/search", function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Product.find({name: {$regex: req.query.string, $options: "i"}}, function (err, docs) {
        var productChumks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChumks.push(docs.slice(i, i + chunkSize));
        }
        fixer.latest({access_key: '3a90f49f66c40c0abe459df4dbac3bfe'})
            .then(function (data) {
                if (req.session.currency === "USD") {
                    cur = 1;
                    sign = '$';
                } else if (req.session.currency === "EUR") {
                    cur = 1 / data.rates['USD'];
                    sign = '€';
                }
                productChumks.forEach(function (products) {
                    products.forEach(function (product) {
                        product.price = product.price * cur;
                        product.price = product.price.toFixed(2);
                        product.cur = sign;
                        product.imagePath = product.image_groups[0].images[0].link;
                    });
                });
            }).then(function () {
            // var breadcrumbs = req.params.id.split('-');
            req.session.oldUrlCart = req.url;
            res.render('shop/products', {
                title: 'Shopping Cart',
                products: productChumks,
                succesMsg: succesMsg,
                noSuccesMsg: !succesMsg,
                csrfToken: req.csrfToken(),
                breadcrumbs: false,
                breadcrumbs_name: "searsch"
            });
        });
    });
});

router.get('/usd', function (req, res, next) {
    req.session.currency = "USD";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});
router.get('/eur', function (req, res, next) {
    req.session.currency = "EUR";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});

router.post('/add-to-cart', function (req, res, next) {
    var productId = req.body._id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, function (err, product) {
        if (err) {
            return res.redirect('/');
        }
        cart.add(product, product.id, req.body.color, req.body.size, req.body.width);
        req.session.cart = cart;
        if (req.session.oldUrlCart) {
            var oldUrl = req.session.oldUrlCart;
            req.session.oldUrlCart = null;
            res.redirect(oldUrl);
        } else {
            res.redirect('/');
        }

    })
});

router.get('/reduce/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.reduceByOne(productId);
    req.session.cart = cart;
    res.redirect('/shoping-cart');
});

router.get('/remove/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.removeItem(productId);
    req.session.cart = cart;
    res.redirect('/shoping-cart');
});


router.get('/shoping-cart', function (req, res, next) {
    if (!req.session.cart) {
        return res.render('shop/shoping-cart', {products: null});
    }
    var cart = new Cart(req.session.cart);
    req.session.oldUrlCart = req.url;
    res.render('shop/shoping-cart', {
        products: cart.generateArray(),
        totalPrice: cart.totalPrice,
        csrfToken: req.csrfToken()
    })
});


router.get('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shoping-cart')
    }
    var error = req.flash('error')[0];
    var cart = new Cart(req.session.cart);
    res.render('shop/checkout', {total: cart.totalPrice, errmsg: error, csrfToken: req.csrfToken()});
});

router.post('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shoping-cart')
    }
    var cart = new Cart(req.session.cart);
    var stripe = require("stripe")("sk_test_hh63HHStusg1fQDO2bVDqyQc");

    stripe.charges.create({
        amount: cart.totalPrice * 100,
        currency: "usd",
        source: req.body.stripeToken,
        description: "Charge from " + req.user.email
    }, function (err, charge) {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/checkout');
        }
        var order = new Order({
            user: req.user,
            cart: cart,
            paymentID: charge.id
        });
        order.save(function (err, result) {
            //to do err
            // console.log(order);
            req.flash('success', 'Successufully bought product!');
            req.session.cart = null;
            res.redirect('/');
        });
    });
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin');
}
