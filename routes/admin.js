var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var multer = require('multer');
var methodOverride = require('method-override')

var Order = require('../models/order');
var Cart = require('../models/cart');
var User = require('../models/user');
var Category = require('../models/category');
var Product = require('../models/product');

var csrfProtection = csrf();

router.use('/', isLoggedInAdmin, function (req, res, next) {
    next();
});
router.use(methodOverride('_method'))
router.post('/add/product/new', function (req, res, next) {
    var array = [], variation_attributes = [];
    var count, i = 0;
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/images/products/large/')
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + '.jpg')
        }
    });

    var upload = multer({storage: storage}).array('image');

    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
        }
        count = req.body.count_image;
        product = new Product({
            image_groups: [{
                view_type: 'large',
                images: array
            }, {
                view_type: 'small',
                images: array
            }
            ],
            variation_attributes: variation_attributes,
            page_description: req.body.page_description,
            page_title: req.body.page_title,
            name: req.body.name,
            primary_category_id: req.body.primary_category_id,
            short_description: req.body.short_description,
            long_description: req.body.long_description,
            price: req.body.price,

        });
        if (req.body.count_color > 0) {
            variation_attributes.push({
                values: [],
                id: 'color',
                name: 'Color',
            });
            for (i = 0; i < req.body.count_color; i++) {
                variation_attributes[variation_attributes.length - 1].values.push({
                    orderable: true,
                    name: req.body['color_' + (i + 1)],
                    value: req.body['color_' + (i + 1)],
                });

            }

        }
        if (req.body.count_size > 0) {
            variation_attributes.push({
                values: [],
                id: 'size',
                name: 'Size',
            });
            for (i = 0; i < req.body.count_size; i++) {
                variation_attributes[variation_attributes.length - 1].values.push({
                    orderable: true,
                    name: req.body['size_' + (i + 1)],
                    value: req.body['size_' + (i + 1)],
                });

            }

        }
        if (req.body.count_width > 0) {
            variation_attributes.push({
                values: [],
                id: 'width',
                name: 'Width',
            });
            for (i = 0; i < req.body.count_width; i++) {
                variation_attributes[variation_attributes.length - 1].values.push({
                    orderable: true,
                    name: req.body['width_' + (i + 1)],
                    value: req.body['width_' + (i + 1)],
                });

            }

        }
        for (i = 0; i < count; i++) {
            var link = req.files[i].path.split('\\');
            array.push({
                alt: req.body['image_' + (i + 1) + '_alt'],
                link: (link[2] + '/' + link[3] + '/' + link[4]),
                title: req.body['image_description_' + (i + 1)],
            });
            if (count.toString() === (i + 1).toString()) {
                product.image_groups[0].images = array;
                product.image_groups[1].images = array;
                product.variation_attributes = variation_attributes;
                product.save(function (err, result) {
                    req.flash('success', 'Successufully add product!');
                    res.redirect('/admin/add/product');
                });

            }

        }


    });
});
router.post('/add/last_category', function (req, res, next) {
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/images/categories')
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + '.jpg')
        }
    });

    var upload = multer({storage: storage}).single('image');

    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
        }
        var link = req.file.path.split('\\');
        Category.find({id: req.body.first_cat}, function (err, category) {
            category[0].categories.forEach(function (category) {
                if (category.id === req.body.second_cat) {
                    category.categories.push({
                        image: (link[2] + '/' + link[3]),
                        id: req.body.second_cat + '-' + req.body.name,
                        name: req.body.name,
                        page_description: req.body.page_description,
                        page_title: req.body.page_title,
                        parent_category_id: req.body.second_cat,
                        c_showInMenu: true
                    });
                }
            });
            category[0].save(function (err, result) {
                req.flash('success', 'Successufully add last category!');
                res.redirect('/admin/add/last_category');
            });
        });
    });
});

router.post('/add/second_category', function (req, res, next) {
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/images/categories')
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + '.jpg')
        }
    });

    var upload = multer({storage: storage}).single('image');

    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
        }
        var link = req.file.path.split('\\');
        Category.find({id: req.body.first_cat}, function (err, category) {
            category[0].categories.push({
                image: (link[2] + '/' + link[3]),
                id: req.body.first_cat + '-' + req.body.name,
                name: req.body.name,
                page_description: req.body.page_description,
                page_title: req.body.page_title,
                parent_category_id: req.body.first_cat,
                c_showInMenu: true
            });
            category[0].save(function (err, result) {
                req.flash('success', 'Successufully add second category!');
                res.redirect('/admin/add/second_category');
            });
        });
    });
});

router.use(csrfProtection);

router.get('/', function (req, res, next) {
    var messages = req.flash('error');
    User.find(function (err, users) {
        Order.find(function (err, orders) {
            res.render('admin/dashboard', {
                csrfToken: req.csrfToken(),
                messages: messages,
                hassErrors: messages.length > 0,
                counUser: users.length,
                countOrders: orders.length,
                title: 'Shopping Cart(admin)',
            })
        });
    });
});

router.get('/users', function (req, res, next) {
    var messages = req.flash('error');
    User.find(function (err, users) {
        res.render('admin/users', {
            title: 'Shopping Cart(admin)',
            csrfToken: req.csrfToken(),
            messages: messages,
            hassErrors: messages.length > 0,
            users: users,
        })
    });
});

router.get('/orders', function (req, res, next) {
    Order.find(function (err, orders) {
        User.find(function (err, users) {
            users.forEach(function (user) {
                orders.forEach(function (val) {
                    if (user._id.toString() === val.user.toString()) {
                        val.userName = user.name;
                    }
                });
            });
            res.render('admin/orders', {
                title: 'Shopping Cart(admin)',
                csrfToken: req.csrfToken(),
                orders: orders,
            })
        });
    });
});
router.get('/add/product', function (req, res, next) {
    var messages = req.flash('error');
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, categories) {
        res.render('admin/add/product', {
            title: 'Shopping Cart(admin)',
            csrfToken: req.csrfToken(),
            categories: categories,
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
        });
    });

});
router.get('/product', function (req, res, next) {
    var messages = req.flash('error');
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, categories) {
        res.render('admin/products', {
            title: 'Shopping Cart(admin)',
            csrfToken: req.csrfToken(),
            categories: categories,
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
        });
    });

});

router.get('/categories', function (req, res, next) {
    var messages = req.flash('error');
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, categories) {
        res.render('admin/categories', {
            title: 'Shopping Cart(admin)',
            csrfToken: req.csrfToken(),
            categories: categories,
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
        });
    });

});

router.get('/add/main_category', function (req, res, next) {
    var messages = req.flash('error');
    var succesMsg = req.flash('success')[0];
    res.render('admin/add/mainCategory', {
        title: 'Shopping Cart(admin)',
        csrfToken: req.csrfToken(),
        succesMsg: succesMsg,
        noSuccesMsg: !succesMsg,
    });

});

router.post('/add/main_category', function (req, res, next) {
    category = new Category({
        id: req.body.name,
        name: req.body.name,
        page_description: req.body.page_description,
        page_title: req.body.page_title,
        parent_category_id: "root",
        c_showInMenu: true
    });
    category.save(function (err, result) {
        req.flash('success', 'Successufully add Main category!');
        res.redirect('/admin/add/main_category');
    });
});

router.get('/add/second_category', function (req, res, next) {
    var messages = req.flash('error');
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, categories) {
        res.render('admin/add/secondCategory', {
            title: 'Shopping Cart(admin)',
            csrfToken: req.csrfToken(),
            categories: categories,
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
        });
    });

});
router.get('/products/:id', function (req, res, next) {
    Product.find({primary_category_id: req.params.id}, function (err, docs) {
        req.session.oldUrlCart = req.url;
        res.send(docs);
    });
});
router.get('/add/last_category', function (req, res, next) {
    var messages = req.flash('error');
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, categories) {
        res.render('admin/add/lastCategory', {
            title: 'Shopping Cart(admin)',
            csrfToken: req.csrfToken(),
            categories: categories,
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
        });
    });

});
router.get('/product/edit/:id',function (req,res) {
    Product.findById(req.params.id, function (err, product) {
        res.render('admin/edit/product', {
            title: 'Shopping Cart',
            product: product,
            ingCount:product.image_groups[0].images.length,
            imgArray:product.image_groups[0].images,

        })
    });
});
router.get('/product/show/:id', function (req, res, next) {
    Product.findById(req.params.id, function (err, product) {
        var large = [];
        product.image_groups.forEach(function (value) {
            if (value.view_type === "large") {
                large = value.images;
            }
        });
        res.render('admin/show/product', {
            title: 'Shopping Cart',
            product: product,
            large: large[0],

        })
    });
});
router.delete('/product/delete', function (req, res, next) {
    Product.findById(req.body.id, function (err, product) {
        product.remove(function (err) {
            Category.find(function (err, categories) {
                res.render('admin/product', {
                    title: 'Shopping Cart(admin)',
                    csrfToken: req.csrfToken(),
                    categories: categories,
                    succesMsg: 'Product has deleted',
                    noSuccesMsg: false,
                });
            });
        });
    })
});

router.delete('/category/delete', function (req, res, next) {
    var id =req.body.id.split('-');
    Category.find({id:id[0]},function (err,category) {
       console.log(category.name);
    });
   /* Product.findById(req.body.id, function (err, product) {
        product.remove(function (err) {
            Category.find(function (err, categories) {
                res.render('admin/product', {
                    title: 'Shopping Cart(admin)',
                    csrfToken: req.csrfToken(),
                    categories: categories,
                    succesMsg: 'Product has deleted',
                    noSuccesMsg: false,
                });
            });
        });
    })*/
});
router.get('/category/:id', function (req, res, next) {
    Category.find({id: req.params.id}, function (err, categories) {
        res.send(categories[0].categories);
    });
});
router.get('/category/:id/:secondId', function (req, res, next) {
    Category.find({id: req.params.id}, function (err, categories) {
        var secondCat = [];
        categories[0].categories.forEach(function (val) {
            if (val.id === req.params.secondId) {
                secondCat = val.categories;
            }
        });
        console.log(secondCat);
        res.send(secondCat);

    });
});
module.exports = router;

function isLoggedInAdmin(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.admin) {
            return next();
        } else
            res.redirect('/user/profile')
    } else
        res.redirect('/user/signin');
}
