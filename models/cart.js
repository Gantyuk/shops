module.exports = function Cart(oldCart) {
    this.items = oldCart.items || {};
    this.totalQty = oldCart.totalQty || 0;
    this.totalPrice = oldCart.totalPrice || 0;

    this.add = function (item, id, color, size, width) {
        var storedItem = this.items[id];
        if (!storedItem) {
            storedItem = this.items[id] = {items: item, qty: 0, price: 0, color: color, size: size, width: width};
        }
        storedItem.qty++;
        storedItem.price = storedItem.items.price * storedItem.qty;
        this.totalQty++;
        this.totalPrice += storedItem.items.price;
    };
    this.reduceByOne = function (id) {
        this.items[id].qty--;
        this.items[id].price -= this.items[id].items.price;
        this.totalQty--;
        this.totalPrice -= this.items[id].items.price;
        if (this.items[id].qty <= 0) {
            delete this.items[id];
        }
    };
    this.removeItem = function (id) {
        this.totalQty -= this.items[id].qty;
        this.totalPrice -= this.items[id].price;
        delete this.items[id];
    };
    this.generateArray = function () {
        var arr = [];
        for (var id in this.items) {
            arr.push(this.items[id]);
        }
        return arr;
    };
};