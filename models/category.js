var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var category = new Schema({
    id: {type: String},
    image: {type: String},
    name: {type: String},
    page_description: {type: String},
    parent_category_id: {type: String},
    c_showInMenu: {type: Boolean}
});
var category_0 = new Schema({
    categories: [category],
    id: {type: String},
    image: {type: String},
    name: {type: String},
    page_description: {type: String},
    page_title: {type: String},
    parent_category_id: {type: String},
    c_showInMenu: {type: Boolean}
});
var category_1 = new Schema({
    categories: [{
        categories: [{
            id: {type: String},
            image: {type: String},
            name: {type: String},
            page_description: {type: String},
            parent_category_id: {type: String},
            c_showInMenu: {type: Boolean}
        }],
        id: {type: String},
        image: {type: String},
        name: {type: String},
        page_description: {type: String},
        page_title: {type: String},
        parent_category_id: {type: String},
        c_showInMenu: {type: Boolean}
    }],
    id: {type: String},
    image: {type: String},
    name: {type: String},
    page_description: {type: String},
    page_title: {type: String},
    parent_category_id: {type: String},
    c_showInMenu: {type: Boolean}
});

module.exports = mongoose.model('Categoty', category_1);